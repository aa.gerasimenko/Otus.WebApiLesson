﻿using Microsoft.AspNetCore.Mvc;
using Otus.WebApiLesson.Models;
using Otus.WebApiLesson.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using HttpGetAttribute = Microsoft.AspNetCore.Mvc.HttpGetAttribute;
using RouteAttribute = Microsoft.AspNetCore.Mvc.RouteAttribute;

namespace Otus.WebApiLesson.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PersonsController : ControllerBase
    {
        private readonly PersonRepository _personRepository;

        public PersonsController(PersonRepository personRepository)
        {
            _personRepository = personRepository;
        }

        [HttpGet]

        public Person GetById(int id)
        {
            throw new HttpResponseException(HttpStatusCode.NotFound);
        }

        [HttpGet("now/actionresult")]
        public IActionResult GetNow()
        {
            return Ok(DateTime.Now);
        }

        [HttpGet("now/task")]
        public Task<DateTime> GetNowTask()
        {
            return Task.FromResult(DateTime.Now);
        }

        [HttpGet("now/value")]
        public DateTime GetNowValue()
        {
            return DateTime.Now;
        }
    }
}
